﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to spawn coins on the road.

public class CoinSpawner : MonoBehaviour
{
	
	public GameObject coin;
	float timer;
	
    void Start()
    {
        timer = Random.Range(4f, 10f);
    }

    void Update()
    {
		
		timer -= Time.deltaTime * Speed.velocity;
		
		//Coins are spawned on random position on the road.
        if(timer <= 0){
			Vector3 coinPosition = new Vector3(Random.Range(-4f, 4f), transform.position.y, transform.position.z);
			Instantiate(coin, coinPosition, transform.rotation);
			
			timer = Random.Range(4f, 10f);
		}
		
		
    }
}
