﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//The purpose of this class is to control behaviour in menu. All button menu button functions are here.

public class MainMenu : MonoBehaviour
{
	
	public enum UpgradeType {control, armor};
	public Text[] buttonsControl, buttonsArmor;
	public GameObject panelHighScores;
	public GameObject panelYourCar;
	public static Text musicButton;
	
    void Start()
    {
		//set of things to be checked and set at the start
		if(PlayerPrefs.GetInt("music") == 1){
			GameObject.Find("Music").GetComponent<AudioSource>().mute = true;
			GameObject.Find("MusicButton").GetComponentInChildren<Text>().text = "Music OFF";
		}
		else{
			GameObject.Find("Music").GetComponent<AudioSource>().mute = false;
			GameObject.Find("MusicButton").GetComponentInChildren<Text>().text = "Music ON";
		}
		UpdateGarageInfo();
        panelHighScores = GameObject.FindGameObjectsWithTag("HighScoresPanel")[0];
        panelHighScores.SetActive(false);
		panelYourCar = GameObject.FindGameObjectsWithTag("YourCarPanel")[0];
        panelYourCar.SetActive(false);
    }

    void Update()
    {
        
    }
	
	//play the game button function
	public void PlayGame(){
		SceneManager.LoadScene("Game");
	}
	
	//high scores panel
	public void OpenHighScores(){	
		panelHighScores.SetActive(true);
	}
	
	public void CloseHighScoresPanel(){	
		panelHighScores.SetActive(false);
	}
	
	//your car panel
	public void OpenYourCar(){	
		panelYourCar.SetActive(true);
	}
	public void CloseYourCar(){	
		panelYourCar.SetActive(false);
	}
	
	//Controls for upgrades
	public void UpgradeControl(int level){
		Upgrade(level, UpgradeType.control);
	}
	
	public void UpgradeArmor(int level){
		Upgrade(level, UpgradeType.armor);
	}
	
	//Upgrades
	public void Upgrade(int level, UpgradeType type){
		
		int numOfCoins = PlayerPrefs.GetInt("coins");
		
		//check and set upgrades
		if(type == UpgradeType.control){
			int currentUpgradeState = PlayerPrefs.GetInt("ControlUpgrades"); //check the state of current upgrade
			if(currentUpgradeState + 1 != level || numOfCoins < level*100)
				return;
			PlayerPrefs.SetInt("ControlUpgrades", currentUpgradeState + 1); //set new upgrade state
			PlayerPrefs.SetInt("coins", numOfCoins - level*100); //subtract from coins correct amount
		}
		else{
			int currentUpgradeState = PlayerPrefs.GetInt("ArmorUpgrades");
			if(currentUpgradeState + 1 != level || numOfCoins < level*100)
				return;
			PlayerPrefs.SetInt("ArmorUpgrades", currentUpgradeState + 1);
			PlayerPrefs.SetInt("coins", numOfCoins - level*100);
		}
		UpdateGarageInfo();
	}
	
	//Update the information of upgrades in Your Car, set buttons color to notify if upgrades are available, not available or already purchased.
	private void UpdateGarageInfo(){
		
		//get information about upgrades
		int control = PlayerPrefs.GetInt("ControlUpgrades");
		int armor = PlayerPrefs.GetInt("ArmorUpgrades");
		
		for(int i=0; i<3; i++){
			if(control > i){
				buttonsControl[i].text = "✓";
				buttonsControl[i].color = Color.green;
			}
			if(control <= i){
				buttonsControl[i].text = "+"+ (i+1) +" for\n"+ (i+1) +"00 Coins";
				buttonsControl[i].color = Color.yellow;
			}
			if(control < i){
				buttonsControl[i].color = Color.grey;
			}
			if(armor > i){
				buttonsArmor[i].text = "✓";
				buttonsArmor[i].color = Color.green;
			}
			if(armor <= i){
				buttonsArmor[i].text = "+"+ (i+1) +" for\n"+ (i+1) +"00 Coins";
				buttonsArmor[i].color = Color.yellow;
			}
			if(armor < i){
				buttonsArmor[i].color = Color.grey;
			}
		}
		//display how many coins user is available to spend
		GameObject.Find("YourCarPanel").transform.Find("TopText").GetComponent<Text>().text = "Here you can upgrade your car! \n Coins available: " + PlayerPrefs.GetInt("coins");
	}
	
	//music on/off
	public void MusicChange(){
		if(PlayerPrefs.GetInt("music") == 0){
			PlayerPrefs.SetInt("music", 1);
			GameObject.Find("Music").GetComponent<AudioSource>().mute = true;
			GameObject.Find("MusicButton").GetComponentInChildren<Text>().text = "Music OFF";
			
		}
		else{
			PlayerPrefs.SetInt("music", 0);
			GameObject.Find("Music").GetComponent<AudioSource>().mute = false;
			GameObject.Find("MusicButton").GetComponentInChildren<Text>().text = "Music ON";
		}
	}
	
	//exit the game button function
	public void ExitGame(){
		Application.Quit();
	}
	
}
