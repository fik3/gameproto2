﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control the car with buttons on screen.

public class MobileControl : MonoBehaviour
{

	//0 = speedup, 1 = slow, 2 = turn right, 3 = turn left;
	public static bool[] presses;

    void Start()
    {
        presses = new bool[4];
    }

    void Update()
    {
        //Debug.Log(presses[0] + "" + presses[1] + "" + presses[2] + "" + presses[3]);
    }
	
	public void ButtonDown(int type){
		
		presses[type] = true;
		
	}
	
	public void ButtonUp(int type){
		
		presses[type] = false;
		
	}
	
}
