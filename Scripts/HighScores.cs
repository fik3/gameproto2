﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//The purpose of this class is to store information about high scores.

public class HighScores : MonoBehaviour
{
    
	public Text displayedName;
	public Text displayedScore;
	
	//class created to store score
	public class Score{
		public string playerName;
		public int playerScore;
		
		public Score(string name = "NoName", int score = 0){
			playerName = name;
			playerScore = score;
		}
		
		//set to string
		public string ToStr(){
			return "" + playerScore + " " + playerName;
		}
		
		//get from string
		public void FromStr(string s){
			string score = s.Split(' ')[0];
			int start = score.Length + 1;
			int length = s.Length - start;
			string name = s.Substring(start, length);
			playerName = name;
			playerScore = int.Parse(score);
		}
	}
	
    void Start()
    {
		//PlayerPrefs.SetString("HighScores", "");
		ShowHighScores();
    }

    void Update()
    {
        
    }
	
	//get score from player prefs
	public static List<Score> GetScoresFromPrefs(){
		
		string highScoresData = PlayerPrefs.GetString("HighScores"); //get data from high scores
		
		List<Score> highScoresTable = new List<Score>(); //list of scores
		
		//if empty - add score
		if(highScoresData == ""){
			for(int i=0; i<10; i++){
				highScoresTable.Add(new Score());
			}
		}
		else{
			foreach(string line in highScoresData.Split('\n')){
				if(line == "")
					continue;
				Score score = new Score();
				score.FromStr(line);
				highScoresTable.Add(score);
			}
		}
		return highScoresTable;
	}
	
	//display scores 
	void ShowHighScores(){
		
		List<Score> highScoresTable = GetScoresFromPrefs(); //get list from player prefs
		string stringScore = "";
		string stringName = "";
		foreach(Score s in highScoresTable){
			stringScore += s.playerScore + "\n";
			stringName += s.playerName + "\n";
		}
		displayedName.text = stringName;
		displayedScore.text = stringScore;
	}
	
	//check if player has got a high score
	public static bool IsHighscore(int score){
		
		List<Score> highScoresTable = GetScoresFromPrefs();
		int worstScore = highScoresTable[9].playerScore;

		return score > worstScore;	
	}
	
	//insert score into high scores table
	public static void InsertScore(string name, int score){
		
		List<Score> highScoresTable = GetScoresFromPrefs(); //get list from player prefs
		
		for(int i=0; i<highScoresTable.Count; i++){
			
			int tempScore = highScoresTable[i].playerScore;
			
			//insert score and remove the worst score from current high scores table
			if(score > tempScore){
				highScoresTable.Insert(i, new Score(name, score));
				highScoresTable.RemoveAt(10);
				break;
			}
			
		}
		
		string fullStr = "";
		foreach(Score s in highScoresTable){
			fullStr += s.ToStr() + "\n";
		}
		
		PlayerPrefs.SetString("HighScores", fullStr);
		
	}
	
}














