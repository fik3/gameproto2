﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//The purpose of this class is to work with scores in game.

public class Scoring : MonoBehaviour
{
	
	public Text scoreText; //text of score
	public Text closeText; //text of additional score for driving close to the enemy
	public static float score = 0f; //score of current run  
	public RectTransform closeRect;
	float closeTimer; //timer for displaying close text
   
    void Start()
    {
        
    }
	
    void Update()
    {
		if(closeTimer > 0){
			closeTimer -= Time.deltaTime;
			closeText.color = Color.Lerp(
				new Color(1f, 0.63f, 0, 0f), new Color(1f, 0.63f, 0, 1f), closeTimer); //text disappearing over time (closeTimer)
			float y = Mathf.Lerp(-50, -150, closeTimer); //change float y over time (closeTimer)
			closeRect.anchoredPosition = new Vector2(0, y);
		}
		//display score in top left corner only when player is alive
		if(CarControl.isAlive){
			scoreText.text = "Score: " + (int)score;
			score += Time.deltaTime * Speed.velocity * 20; //add to score over time and depending on speed
		}
		else{
			scoreText.gameObject.SetActive(false);
		}
    }
	
	//display close text on the screen and add points to total score
	public void DisplayClose(){
		
		score += 100f;
		closeText.color = new Color(1f, 0.63f, 0, 1f);
		closeRect.anchoredPosition = new Vector2(0, -150);
		closeTimer = 1;
	
	}
	
}
