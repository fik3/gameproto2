﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control the music in game mode.

public class MusicControl : MonoBehaviour
{
	
	public int music;
	
    void Start()
    {
        music = PlayerPrefs.GetInt("music");
		gameObject.GetComponent<AudioSource>().mute = music == 1;
    }

    void Update()
    {
        
    }
}
