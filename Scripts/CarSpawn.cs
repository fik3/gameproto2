﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control the spawning of cars on the road.

public class CarSpawn : MonoBehaviour
{

    public GameObject[] cars;
    float timer;

    void Start()
    {
        timer = Random.Range(0.8f, 1.5f); //timer used to spawn cars in different time
    }

    
    void Update()
    {

        timer -= Time.deltaTime * Speed.velocity; //if player speeds up cars will be spawned faster, if player slows down - slower.

        if(timer <= 0)
        {
			
			//spawn 1 or 2 cars (if 2 - spawn on different lanes)
			int numberOfCars = Random.Range(1, 3);
			int lastLane = -1;
			
			while(numberOfCars-- > 0){
				
				int lane;
				
				do{
					lane = Random.Range(0, 4);
				}while(lane == lastLane);
		
				int carNumber = Random.Range(0, 6); //choose one of 6 cars available
				
				//lanes are set to exact positions
				if (lane == 0)
				{ 
					Vector3 carPosition = new Vector3(4f, transform.position.y, transform.position.z);
					Instantiate(cars[carNumber], carPosition, transform.rotation);
				}
				if (lane == 1)
				{
					Vector3 carPosition = new Vector3(1.3f, transform.position.y, transform.position.z);
					Instantiate(cars[carNumber], carPosition, transform.rotation);
				}
				if (lane == 2)
				{
					Vector3 carPosition = new Vector3(-1.23f, transform.position.y, transform.position.z);
					Instantiate(cars[carNumber], carPosition, transform.rotation);
				}
				if (lane == 3)
				{
					Vector3 carPosition = new Vector3(-3.86f, transform.position.y, transform.position.z);
					Instantiate(cars[carNumber], carPosition, transform.rotation);
				}
				
				lastLane = lane;
				
				timer = Random.Range(0.8f, 1.5f);
			}
        }
        
    }
}
