﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control cars switching lanes.

public class AITurning : MonoBehaviour
{
    float timer = 2f;
    int state = 0; //0 = timer, 1 = turn
    int whereToGo;
	float endPosition;
	float chance;

	//returns int to control which way the car should turn
    int WhereToGo(){
		
		int whichDir = Random.Range(0, 2);
		
        RaycastHit hit;
		float x = transform.position.x;
		
		//make sure the car will not move outside the road and will not hit other car
		if(whichDir == 0){
			if(x > -3.5f && !Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hit, 2f))
				return -1;
			if(x < 3.5f && !Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 2f))
				return 1;
		}
		else{
			if(x < 3.5f && !Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 2f))
				return 1;
			if(x > -3.5f && !Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hit, 2f))
				return -1;
		}
        return 0;
    }

    void Start()
    {
		chance = Random.Range(0f, 1f); //chance of car turning
    }

    void Update()
    {
		if(chance > 0.05f * Speed.speedOverTime)
			return;
        switch(state){
            case 0:
                if(timer > 0){
                    timer -= Time.deltaTime * Speed.velocity;
                }
                else{
                    whereToGo = WhereToGo();
                    if(whereToGo == 0)
                        state = 2;
                    else{
						endPosition = transform.position.x + whereToGo * 2.7f;
                        state = 1;
					}
                }
                break;
            case 1:
                if(Mathf.Abs(transform.position.x - endPosition) > 0.1f)
                    transform.Translate(whereToGo * Time.deltaTime * 2.7f, 0, 0); //move the car
                else
                    state = 2;
                break;
        }
    }
}
