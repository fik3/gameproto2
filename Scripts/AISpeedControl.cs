﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control the speed of cars appearing on the road. 

public class AISpeedControl : MonoBehaviour
{
    
	bool alreadySlowed = false;
	
	float timer;
    
    void Start()
    {
		timer = 0.25f; //As Raycasts are really overwhelming for the system, the timer is set to run Raycasts every 0.25 second.
    }

    void Update()
    {
		if(timer < 0){
			//if the car is already slowed - do nothing
			if(alreadySlowed)
				return;
			Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.back) * 5f, Color.yellow); //visible Raycast drawn.
			
			RaycastHit hit;
			
			//Raycast to control the collision of the enemy car with other enemy cars. If the raycast hits another car it
			//gets the other car's speed component and overwrites its own with this value.
			if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), out hit, 5f)){
				if(hit.transform.tag == "Enemy Car"){
					float speedOfCar = hit.transform.GetComponent<EnemyCarMovement>().speed;
					transform.GetComponent<EnemyCarMovement>().speed = speedOfCar;
					alreadySlowed = true;
					
					//If the car is in shorter distance than length of the Raycast - move it back.
					if(hit.distance < 5f){
						transform.Translate(0, 0, 5f - hit.distance);
					}
				}
			}
			timer = 0.25f;
		}
		else
			timer -= Time.deltaTime;
    }
}
