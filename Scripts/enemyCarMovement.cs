﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to store information about movement of cars spawned on a road and trees.

public class EnemyCarMovement : MonoBehaviour
{

    public float speed = 5f;
	public float randomValue = 0;

    void Start()
    {
        float r = Random.Range(-randomValue, randomValue);
		speed += r;
    }

	//Change the speed based on the road's speed. If player accelerates or slows down also change speed.
    void Update()
    {
        transform.Translate(new Vector3(0, 0, 1) * speed * Speed.velocity * Time.deltaTime * Speed.speedOverTime);
        if (transform.position.z > 14)
        {
            Destroy(gameObject);
        }
    }
}
