﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control the speed of the road (and grass)

public class RoadMoving : MonoBehaviour
{

    public float speed;
    float offset;

    void Start()
    {
        
    }

    
    void Update()
    {
		//use values from Speed class to move the road and grass correctly
        offset += Time.deltaTime * speed * Speed.speedOverTime * Speed.velocity;
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (0, offset);
    }
}
