﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayAgainMenu : MonoBehaviour
{
    
    public static GameObject panel;
	public static GameObject promptForName;
	public static GameObject controlPanel;
	public static Text finalScore;
	public static Text finalScore2;
	public static Text name;
	
	void Start()
    {
		//Set of panels and components to be found.
		controlPanel = GameObject.Find("ControlPanel");
		controlPanel.SetActive(true);
		panel = GameObject.FindGameObjectsWithTag("SuperPanel")[0];
        panel.SetActive(false);
		finalScore = panel.transform.Find("FinalScore").GetComponent<Text>();
		promptForName = GameObject.FindGameObjectsWithTag("ExtraPanel")[0];
        promptForName.SetActive(false);
		finalScore2 = promptForName.transform.Find("FinalScore").GetComponent<Text>();
		name = promptForName.transform.Find("InputField").Find("Text").GetComponent<Text>();
    }

    void Update()
    {
        
    }
	
	//Reset the game (play again button)
	public void ResetGame(){
		
		Scoring.score = 0;
		SceneManager.LoadScene("Game");
		
	}
	
	//Go back to menu (main menu button)
	public void BackToMenu(){
		
		Scoring.score = 0;
		SceneManager.LoadScene("Menu");
		
	}
	
	//setting the final score panel to active
	public static void DisplayPanel(){
		
		controlPanel.SetActive(false);
		promptForName.SetActive(false);
		panel.SetActive(true);
		finalScore.text = "" + (int)Scoring.score;
		
	}
	
	//setting the panel that asks for name to active
	public static void AskForName(){
		
		controlPanel.SetActive(false);
		promptForName.SetActive(true);
		finalScore2.text = "" + (int)Scoring.score;
		
	}
	
	//saving the score (OK button)
	public void SaveScore(){
		//input field cannot be empty
		if(name.text == "")
			return;
		//save the score into high scores table
		HighScores.InsertScore(name.text, (int)Scoring.score);
		DisplayPanel();
	}
	
}
