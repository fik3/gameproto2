﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to control trees spawning. 

public class TreeSpawner : MonoBehaviour
{
    public GameObject[] trees;
    float timer;

    void Start()
    {
        timer = Random.Range(0.1f, 0.3f); //Spawn trees in different time.
		SpawnAtStart(); //Spawn set of trees immediately when the game starts.
    }
	
	//Chooses number of trees to be spawned when the game starts and their positions.
	void SpawnAtStart()
	{
		float z = 0;
		while(z > -80){
			
			int numberOfTrees = Random.Range(3, 9); //choose number of trees to be spawned
			
			while(numberOfTrees-- > 0){
				
				float lane = Random.Range(-75f, 75f); //width of the grass
				lane = lane > 0 ? lane + 8 : lane - 8; //condition to avoid spawning trees on a road
            
				int treeNumber = Random.Range(0, 1);
				
					Vector3 treePosition = new Vector3(lane, transform.position.y, z);
					Instantiate(trees[treeNumber], treePosition, transform.rotation);
				
				z -= Random.Range(1f, 2f);
			}
		}
	}
    
	//Spawn trees during the game.
    void Update()
    {

        timer -= Time.deltaTime * Speed.velocity; //spawn trees during the game also depends on speed

        if(timer <= 0)
        {
			
			int numberOfTrees = Random.Range(3, 9);
			
			while(numberOfTrees-- > 0){
				
				float lane = Random.Range(-75f, 75f); 
				lane = lane > 0 ? lane + 8 : lane - 8;
            
				int treeNumber = Random.Range(0, 1);
				
					Vector3 treePosition = new Vector3(lane, transform.position.y, transform.position.z);
					Instantiate(trees[treeNumber], treePosition, transform.rotation);
				
				timer = Random.Range(0.1f, 0.3f);
			}
        }
        
    }
}
