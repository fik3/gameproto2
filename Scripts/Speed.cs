﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The purpose of this class is to store information about speed in game.

public class Speed: MonoBehaviour
{
    
	float controlUpgrade; //checks 
    public static float speedOverTime = 1; //speed over time period
    public static float velocity = 1; //speeding up, slowing down
    public float acceleration;

    void Start()
    {
		//checks the upgrades of player's car
		controlUpgrade = 0.5f + 0.5f * PlayerPrefs.GetInt("ControlUpgrades");
        speedOverTime = 1;
		velocity = 1;
    }

    void Update()
    {
		//increase speedOverTime
        speedOverTime += acceleration * Time.deltaTime;
        
		//if (Input.GetKey(KeyCode.UpArrow))
		//^old input with keyboard
	
		//if correct button pressed - speed up, if correct button pressed - slow down. If nothing - return to the original speed.
		if (MobileControl.presses[0])
        {
            velocity += 0.25f * Time.deltaTime * controlUpgrade;
            if (velocity > 1.5f)
                velocity = 1.5f;
        }
        else
        {
            //if (Input.GetKey(KeyCode.DownArrow))
			//^old input with keyboard
			if (MobileControl.presses[1])
            {
                velocity -= 0.5f * Time.deltaTime * controlUpgrade;
                if (velocity < 0.7f)
                    velocity = 0.7f;
            }
            else
            {
                velocity = (velocity * 0.98f + 1f * 0.02f); //return to original speed
            }
        }

        //Debug.Log(velocity + " " + speedOverTime); 

    }
}
