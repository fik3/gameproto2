﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//The purpose of this class is to control the car and manage things connected with it like scoring, coin collecting and collisions.

public class CarControl : MonoBehaviour
{
	
	float blinkTimer = 0; //timer for car blinking when hits enemy car
	int lives; //lives in game
	float upgradedSpeed; //speed of turning the car after upgrades
    public float carSpeed; //how fast the car is turning
	public GameObject explosion; //explosion of car
    Vector3 position;
	public static bool isAlive;

    void Start()
    {
		//set of things to be checked and set at start
		lives = PlayerPrefs.GetInt("ArmorUpgrades") + 1;
		checkLives();
		upgradedSpeed = carSpeed + PlayerPrefs.GetInt("ControlUpgrades") * 0.5f;
		GameObject.Find("Coins").GetComponent<Text>().text = "Coins: " + PlayerPrefs.GetInt("coins");
		isAlive = true;
        position = transform.position;
    }

   
    void Update()
    {
		if(blinkTimer > 0)
			blinkTimer -= Time.deltaTime * 5;
		else
			blinkTimer = 0;
		GetComponent<Renderer>().enabled = Mathf.FloorToInt(blinkTimer) % 2 == 0; //blink the car 
		
        //position.x += Input.GetAxis ("Horizontal") * upgradedSpeed * Time.deltaTime * -1;
		//^ old input for car turning with keyboard
		
		int posx = 0; //int for turning the car
		
		if(MobileControl.presses[2])
			posx += 1;
		if(MobileControl.presses[3])
			posx -= 1;
		position.x += posx * upgradedSpeed * Time.deltaTime * -1;
        position.x = Mathf.Clamp(position.x, -4f, 4f); //restriction on player movement
        transform.position = position;
    }

	//collision with enemy cars
    private void OnCollisionEnter(Collision collision)
    {
		if(blinkTimer > 0)
			return;
        if (collision.gameObject.tag == "Enemy Car")
        {
			//if player hits enemy car, check how many lives are left. If player has more - set blinkTimer and subtract one life, otherwise destroy player's car.
			lives--;
			checkLives();
			blinkTimer = 6;
			if(lives > 0)
				return;
			Instantiate(explosion, transform.position, transform.rotation); //explosion animation
            Destroy(gameObject); //destroy car
			
			//displays panel that prompts for the name only if the score should be saved as high score
			if(HighScores.IsHighscore((int)Scoring.score)){
				PlayAgainMenu.AskForName();
			}
			else{
				PlayAgainMenu.DisplayPanel();
			}
			isAlive = false;
			
        }
		
		//collecting coins 
		if(collision.gameObject.name == "coin Variant"){
			Destroy(collision.gameObject);
			PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 1);
			GameObject.Find("Coins").GetComponent<Text>().text = "Coins: " + PlayerPrefs.GetInt("coins");
		}
		
    }
	
	//check how many lives player has and add to string that displays them
	private void checkLives(){
		string livesStr = "";
			for(int i=0; i<lives; i++)
				livesStr += "❤️";
			GameObject.Find("Lives").GetComponent<Text>().text = livesStr;
	}
	
	//check if player drives close to enemy car
	private void OnTriggerEnter(Collider collider){
		if(blinkTimer > 0) //do nothing if player is blinking
			return;
		if (collider.tag == "Enemy Car")
        {
			FindObjectsOfType<Scoring>()[0].DisplayClose();
        }
	}

}
